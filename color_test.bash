#!/bin/bash

set import bash

echo "\033[0;30m Black\033[m"
echo "\033[0;31m Red\033[m"
echo "\033[0;32m Green\033[m"
echo "\033[0;33m Badami\033[m"
echo "\033[0;34m Blue\033[m"
echo "\033[0;35m Beguni\033[m"
echo "\033[0;36m sayan\033[m"
echo "\033[0;37m halka dhusor\033[m"
echo 
echo "\033[1;30m Garo dhusor\033[m"
echo "\033[1;31m Halka lal\033[m"
echo "\033[1;32m halka sobuj\033[m"
echo "\033[1;33m holud\033[m"
echo "\033[1;34m halka nill\033[m"
echo "\033[1;35m halka beguni\033[m"
echo "\033[1;36m halka soyan\033[m"
echo "\033[1;37m sada\033[m"
echo 
echo "\033[0;40m টককালকা\033[m"
echo "\033[0;41m লকাল\033[m"
echo "\033[0;42m সববজ\033[m"
echo "\033[0;43m বকামদকাম\033[m"
echo "\033[0;44m ননীল\033[m"
echo "\033[0;45m টবগুননী\033[m"
echo "\033[0;46m সববজকাভ ননীল বকা সকাযকান(Cyan)\033[m"
echo "\033[0;47m হকালককা ধধূসর\033[m"

red='\033[1;91m'
green='\033[1;92m'
yellow='\033[1;93m'
blue='\033[1;94m'
purple='\033[1;95m'
cyan='\033[1;96m'
white='\033[1;97m'

stop='\33[m' # this is for stop statement 
#echo "\033[0;41m Background \33[m"
printf "\033[0;41m Background \33[m"

RS="\[\033[0m\]" # reset
HC="\[\033[1m\]" # hicolor 
UL="\[\033[4m\]" # underline
INV="\[\033[7m\]" # inverse background and foreground
FBLK="\[\033[30m\]" # foreground black
FRED="\[\033[31m\]" # foreground red
FGRN="\[\033[32m\]" # foreground green
FYEL="\[\033[33m\]" # foreground yellow
FBLE="\[\033[34m\]" # foreground blue
FMAG="\[\033[35m\]" # foreground magenta
FCYN="\[\033[36m\]" # foreground cyan
FWHT="\[\033[37m\]" # foreground white
BBLK="\[\033[40m\]" # background black
BRED="\[\033[41m\]" # background red
BGRN="\[\033[42m\]" # background green
BYEL="\[\033[43m\]" # background yellow
BBLE="\[\033[44m\]" # background blue
BMAG="\[\033[45m\]" # background magenta
BCYN="\[\033[46m\]" # background cyan
BWHT="\[\033[47m\]" # background white

main () {
	# this is a main function
	printf "\n$red"; printf "a script by Farhan Sadik\n"; printf "$stop";
	figlet "ColorUI"
}

footer() { 
    echo; printf "$green" && printf "Simple Color Script$yellow v0.1\n";
    printf "$green" && printf "Created by$red Farhan Sadik\n"; 
    printf "$green" && printf "Square Development Group\n"; echo && exit 0; 
}

if main
	then footer #simple_test
	else sleep 0;
fi
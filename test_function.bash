#!/bin/bash

set import bash from busybox 
log=my_output.log;

	# script by farhan sadik
	# once i used to test my coloring script at here

main() {
	printf "Hello World!\n";
}

function style() {
	echo -e "  \e[5mEdit the line that begins with GRUB_THEME\e[0m"
    echo -e "  \e[7mGRUB_THEME=/boot/grub/themes/breeze/theme.txt\e[0m"
    echo -e "\e[1m\e[32m==> \e[97mApplying changes...\e[0m"
    echo -e "\e[1m\e[34m  -> \e[97mTheme successfully applied!"
    echo -e "\e[1m\e[34m  -> \e[97mRestart your PC to check it out."
}

if main; 
	then style
	else printf "failed to run script\n";
fi
